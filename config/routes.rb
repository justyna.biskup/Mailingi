Rails.application.routes.draw do
  resources :articles
  resources :mailing_groups
  resources :other_groups
  resources :level_groups
  resources :subject_groups
  resources :mailing_statuses
  resources :user_groups
  resources :users
  root 'welcome#index'




resources :mailings do
    member do
      get :edit_comment
      patch :save_comment
      get :copy
    end


    resources :group_files
    resources :mailing_files
  end


  resources :sessions, only: [:new, :create, :destroy]

  match "/register", to: "users#new", via: "get"
  match '/signin',  to: 'sessions#new',         via: 'get'
  match '/signout', to: 'sessions#destroy',     via: 'delete'

  match "/calendar", to: "mailings#calendar", via: "get"
  match "/holidays", to: "mailings#holidays", via: "get"
  match "/timeline", to: "mailings#timeline", via: "get"

  get 'group_files/:id/:style/:basename.:extension' => 'group_files#render_file'
  get 'mailing_files/:id/:style/:basename.:extension' => 'mailing_files#render_file', :constraints => { :basename => /.*/ }



  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
