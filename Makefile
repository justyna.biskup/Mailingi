up:
	git pull
	bundle install
	RAILS_ENV=production bundle exec rake db:migrate assets:precompile
	bundle exec whenever --update-crontab
	touch tmp/restart.txt

