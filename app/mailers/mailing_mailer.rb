class MailingMailer < ApplicationMailer
 
  def task_email(mailing, user)
    @mailing = mailing
    @user = user
    mail(to: @user.email, subject: "Przypisano do Ciebie mailing nr #{@mailing.id}.")
  end

  def change_email(mailing, user)
    @mailing = mailing
    @user = user
    mail(to: @user.email, subject: "Przypisany do Ciebie mailing (nr #{@mailing.id}) został zmieniony.")
  end

  def group_email(mailing, user)
    @mailing = mailing
    @user = user
    mail(to: @user.email, subject: "Stwórz grupę do mailingu nr #{@mailing.id}.")
  end

  def change_group_email(mailing, user)
    @mailing = mailing
    @user = user
    mail(to: @user.email, subject: "Zmień grupę do mailingu nr #{@mailing.id}.")
  end

  def reminder_group_email(mailing, user)
    @mailing = mailing
    @user = user
    mail(to: @user.email, subject: "Jutro wysyłka! Stwórz grupę do mailingu nr #{@mailing.id}.")
  end

  def send_email(mailing, user)
    @mailing = mailing
    @user = user
    mail(to: @user.email, subject: "Mailing nr #{@mailing.id} został wysłany.")
  end


  def destroy_file_email(mailing, user)
    @mailing = mailing
    @user = user
    mail(to: @user.email, subject: "Usunięto plik w mailingu nr #{@mailing.id}.")
  end

  def add_group_email(mailing, user)
    @mailing = mailing
    @user = user
    mail(to: @user.email, subject: "Dodano grupę do mailingu nr #{@mailing.id}.")
  end

  def destroy_group_email(mailing, user)
    @mailing = mailing
    @user = user
    mail(to: @user.email, subject: "Usunięto grupę w mailingu nr #{@mailing.id}.")
  end

end
