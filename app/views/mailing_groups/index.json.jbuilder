json.array!(@mailing_groups) do |mailing_group|
  json.extract! mailing_group, :id, :mailing_id, :level_group_id, :subject_group_id, :other_group_id
  json.url mailing_group_url(mailing_group, format: :json)
end
