json.array!(@subject_groups) do |subject_group|
  json.extract! subject_group, :id, :name
  json.url subject_group_url(subject_group, format: :json)
end
