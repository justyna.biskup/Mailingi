json.array!(@other_groups) do |other_group|
  json.extract! other_group, :id, :name
  json.url other_group_url(other_group, format: :json)
end
