json.array!(@mailing_statuses) do |mailing_status|
  json.extract! mailing_status, :id, :name
  json.url mailing_status_url(mailing_status, format: :json)
end
