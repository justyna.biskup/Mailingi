json.array!(@mailings) do |mailing|
  json.extract! mailing, :id, :name, :description, :send_date, :write_date, :code_date, :group_date, :status_id, :preview, :assign_to, :author, :note_id, :priority, :has_group
  json.url mailing_url(mailing, format: :json)
end