json.array!(@mailings) do |mailing|
  json.extract! mailing, :id, :name, :description, :send_date, :mailing_status
  json.title mailing.name + " (#" + mailing.id.to_s + ")"
  json.description mailing.description
  json.start mailing.send_date
  json.end mailing.send_date
  json.mailing_status mailing.mailing_status
  json.url mailing_url(mailing)
  json.color calendar_colors(mailing, current_user)
end