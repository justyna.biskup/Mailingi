class OtherGroupsController < ApplicationController
  before_action :set_other_group, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @other_groups = OtherGroup.all
    respond_with(@other_groups)
  end

  def show
    respond_with(@other_group)
  end

  def new
    @other_group = OtherGroup.new
    respond_with(@other_group)
  end

  def edit
  end

  def create
    @other_group = OtherGroup.new(other_group_params)
    @other_group.save
    respond_with(@other_group)
  end

  def update
    @other_group.update(other_group_params)
    respond_with(@other_group)
  end

  def destroy
    @other_group.destroy
    respond_with(@other_group)
  end

  private
    def set_other_group
      @other_group = OtherGroup.find(params[:id])
    end

    def other_group_params
      params.require(:other_group).permit(:name)
    end
end
