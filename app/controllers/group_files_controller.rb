class GroupFilesController < ApplicationController
  respond_to :html, :xml, :json
  before_action :set_group_file, only: [:show, :edit, :update, :destroy]
  
  def render_file
    group_file = GroupFile.find(params[:id])
    send_file group_file.file.path(params[:style]), :type => group_file.file.content_type
  end

  # GET /group_files
  # GET /group_files.json
  def index
    @mailing = Mailing.find(params[:mailing_id])
    @group_files = @mailing.group_files

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @group_files }
    end
  end

  # GET /group_files/1
  # GET /group_files/1.json
  def show
    @group_file = GroupFile.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @group_file }
    end
  end

  # GET /group_files/new
  # GET /group_files/new.json
  def new
    @mailing = Mailing.find(params[:mailing_id])
    @group_file = @mailing.group_files.build

    respond_modal_with @group_file, location: root_path
  end

  # GET /group_files/1/edit
  def edit
    @mailing = Mailing.find(params[:mailing_id])
    @group_file = GroupFile.find(params[:id])

    respond_modal_with @group_file, location: root_path
  end

  # POST /group_files
  # POST /group_files.json
  def create
    @mailing = Mailing.find(params[:mailing_id])



    unless params[:group_file][:files].nil?
      params[:group_file][:files].each do |document|
        @group_file = GroupFile.create(
            file: document,
            mailing_id: params[:group_file][:mailing_id],
            group_size: params[:group_file][:group_size],
        )
      end
      

    @send_user = @group_file.mailing.assign_to_id
    unless @send_user.nil?
      MailingMailer.add_group_email(@group_file.mailing, @send_user).deliver_now
    end

      redirect_to mailing_path(@group_file.mailing, anchor: "group_files")
    else
      @group_file = @mailing.group_files.build
      @group_file.errors[:files] = "Wybierz przynajmniej jeden plik"

      respond_modal_with @group_file, location: root_path
    end
  end

  # PUT /group_files/1
  # PUT /group_files/1.json
  def update
    @group_file = GroupFile.find(params[:id])

    unless params[:group_file][:files].nil?
      params[:group_file][:files].each do |document|
        @group_file.file = document
      end
    end

    respond_to do |format|
      if @group_file.update(group_file_params)
        format.html { redirect_to mailing_path(@group_file.mailing, anchor: "group_files"), notice: 'GroupFile was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @group_file.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /group_files/1
  # DELETE /group_files/1.json
  def destroy
    @group_file = GroupFile.find(params[:id])
    @group_file.destroy

    @send_user = @group_file.mailing.assign_to_id
    unless @send_user.nil?
      MailingMailer.destroy_group_email(@group_file.mailing, @send_user).deliver_now
    end

    respond_to do |format|
      format.html { redirect_to mailing_path(@group_file.mailing, anchor: "group_files") }
      format.js
    end
  end

  private

    def set_group_file
      @group_file = GroupFile.find(params[:id])
    end

    def group_file_params
      params.require(:group_file).permit(:document, :group_size)
    end
end
