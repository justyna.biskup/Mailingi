// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery.turbolinks
//= require jquery_ujs
//= require jquery-ui/core
//= require jquery-ui/draggable
//= require turbolinks
//= require bootstrap
//= require bootstrap-datepicker/core
//= require bootstrap-datepicker/locales/bootstrap-datepicker.pl.js
//= require moment
//= require fullcalendar
//= require fullcalendar/lang/pl
//= require jquery.slimscroll.min
//= require fancybox
//= require trumbowyg/trumbowyg
//= require trumbowyg/langs/pl
//= require app
//= require_tree .



 var AdminLTEOptions = {
    //Enable sidebar expand on hover effect for sidebar mini
    //This option is forced to true if both the fixed layout and sidebar mini
    //are used together
    sidebarExpandOnHover: false,
    //BoxRefresh Plugin
    enableBoxRefresh: true,
    //Bootstrap.js tooltip
    enableBSToppltip: true,

    sidebarSlimScroll: false,
  };




$(window).bind('page:change', function() {
    var datepickerOpts = {
        format: "yyyy-mm-dd",
        daysOfWeekHighlighted: "0,6",
        language: "pl",
        todayHighlight: true,
        orientation: "bottom left",
        clearBtn: true
    };
    $("input.date").datepicker(datepickerOpts);
    $('.input-daterange').datepicker(datepickerOpts);


    $('#calendar:not(".fc-event")').on('contextmenu', function (e) {
        e.preventDefault()
    });


    $('#calendar').fullCalendar({
        header: {
            right: 'today, prev,next',
            center: 'title',
            left: 'month,basicWeek'
        },
        lang: "pl",
        eventSources: [
            {
                url: '/holidays.json',
                color: '#fff',
                textColor: '#555',
                editable: false
            },
            {
                url: '/calendar.json'
            }
        ],
        eventRender: function(event, element) {
            element.attr('title', event.title);
            element.bind('mousedown', function (e) {
                if (e.which == 3) {
                    var contextCopyMenu = $("<div id='rightCopyMenu' class='open addMailingContextCopyMenu'><ul class='dropdown-menu'><li><a href='/mailings/" + event.id + "/copy'>kopiuj mailing</a></li></ul></a>");
                    contextCopyMenu.css({
                        "top": e.pageY + "px",
                        "left": e.pageX + "px",
                    });
                    $("#rightCopyMenu").remove();
                    $("#rightContextMenu").remove();
                    $("body").append(contextCopyMenu);

                }
            });
        },
        editable: true,
        eventLimit: true,
        selectable: false,
        dragOpacity: "0.5",
        eventDurationEditable: false,
        eventDrop: function(event, dayDelta, minuteDelta, allDay, revertFunc) {
            if (!confirm("Na pewno zmienić datę mailingu: '" + event.title + "'?")) {
                revertFunc();
            } else {
                updateEvent(event);
            }
        },
        dayRightclick: function(date, jsEvent, view) {
            var contextMenu = $("<div id='rightContextMenu' class='open addMailingContextMenu'><ul class='dropdown-menu'><li><a href='/mailings/new?mailing[send_date]=" + date.format('YYYY-MM-DD') + "'>dodaj mailing " + date.format('YYYY-MM-DD') + "</a></li></ul></a>");
            contextMenu.css({
                "top": jsEvent.pageY + "px",
                "left": jsEvent.pageX + "px",
            });
            $("#rightCopyMenu").remove();
            $("#rightContextMenu").remove();
            $("body").append(contextMenu);

            return false;
        }
     
    });

    function updateEvent(the_event) {
      $.ajax({
        url: "/mailings/" + the_event.id,
        type: "PUT",
        dataType: "json",
        data: {
          mailing: {
            send_date: "" + new Date(the_event.start).toUTCString(),
          }
        }
      })
    }


    $("html").on("click", function(event){
        if(event.button!==2) {
            $("#rightCopyMenu").remove();
            $("#rightContextMenu").remove();
        }
    })


    $("a.fancybox").fancybox();


    $('[data-toggle="tooltip"]').tooltip();

    $('#mailing_description').trumbowyg({
        lang: 'pl',
        autogrow: true,
        convertLink: true,
    });


});