

class @RowCloner
    # Dostępne opcje:
    # options:
    # - duplicateCallback: ($clone, this) calback odpalany na klonowanym elemencie
    # - removeCallback: ($row) callback odpalany na usuwanym wierszu
    constructor: (rowSelector, options) ->
        @container = $(rowSelector).parent()
        @rowSelector = rowSelector
        @options = options || {}
        @rowClone = $(@rowSelector+':first').clone()
        $("input, select", @rowClone).val('')
        $("select option[selected]", @rowClone).removeAttr('selected')
        $("select option:first", @rowClone).attr('selected', 'selected')


    duplicate: (e) ->
        e.preventDefault()
        $clone         = $( $(@rowClone).clone() )
        startingFrom   = parseInt( $( $('select, input', $clone)[0] ).attr('name').match(/\[(\d+)]/)[1] ) || 0
        formsOnPage    = $(@rowSelector).length + startingFrom

        $clone.find('label').each ->
            oldLabel = $(this).attr 'for'
            newLabel = oldLabel.replace(new RegExp(/_[0-9]+_/), "_#{formsOnPage}_")
            $(this).attr 'for', newLabel

        $clone.find('select, input').each ->
            oldId = $(this).attr('id') || ""
            newId = oldId.replace(new RegExp(/_[0-9]+_/), "_#{formsOnPage}_")
            $(this).attr 'id', newId

            oldName = $(this).attr 'name'
            newName = oldName.replace(new RegExp(/\[[0-9]+\]/), "[#{formsOnPage}]")
            $(this).attr 'name', newName

        if(@options.duplicateCallback)
            @options.duplicateCallback($clone, this)

        @container.append($clone)

    remove: (e) ->
        e.preventDefault()
        $row = $(e.target).closest(@rowSelector)
        $(".destroy-checkbox", $row).prop('checked', true);
        $row.slideUp()
