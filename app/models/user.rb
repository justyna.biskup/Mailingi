class User < ActiveRecord::Base
	has_secure_password

	validates :name, presence: true, length: {minimum: 5, maximum: 50}
	VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
	validates :email, presence: true, format: {with: VALID_EMAIL_REGEX}, uniqueness: {case_sensitive: false}
	validates :password, length: {minimum: 6}, :if => :validate_password?

	has_many :mailings
	has_many :mailing_notes

	belongs_to :user_group_id, class_name: "UserGroup", foreign_key: "group_id"
	

	before_save { self.email = email.downcase }

	# sprawdzanie czy należy do grupy piszących
	def writer?
		return group_id == 2
	end

	# sprawdzanie czy należy do grupy HTMLujących
	def coder?
		return group_id == 3
	end

	private

	  	def validate_password?
	    	password.present? || password_confirmation.present?
	  	end

end
