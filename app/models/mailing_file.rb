class MailingFile < ActiveRecord::Base

	belongs_to :mailing
    audited associated_with: :mailing

	has_attached_file :file,
					styles: lambda { |a| (a.instance.is_image? or a.instance.is_pdf?) ? { thumb: ["100x100>", :jpg], medium: ["300x300>", :jpg] }
						: {}
					},
                	:processors => lambda { |a| a.is_video? ? [ :ffmpeg ] : [ :thumbnail ] },
                    convert_options: {all: '-alpha remove -background white'},
                    :url => "/:class/:id/:style/:basename.:extension",
                    :path => ":rails_root/private/:class/:attachment/:id_partition/:style/:filename",
					:default_url => "/mailing_files/files/:style/missing.png"


  do_not_validate_attachment_file_type :file

        def is_video?
                file.instance.file_content_type =~ %r(video)
        end

        def is_image?
                file.instance.file_content_type =~ %r(image)
        end

        def is_pdf?
                file.instance.file_content_type =~ %r(application/pdf)
        end

end
