require 'test_helper'

class LevelGroupsControllerTest < ActionController::TestCase
  setup do
    @level_group = level_groups(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:level_groups)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create level_group" do
    assert_difference('LevelGroup.count') do
      post :create, level_group: { name: @level_group.name }
    end

    assert_redirected_to level_group_path(assigns(:level_group))
  end

  test "should show level_group" do
    get :show, id: @level_group
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @level_group
    assert_response :success
  end

  test "should update level_group" do
    patch :update, id: @level_group, level_group: { name: @level_group.name }
    assert_redirected_to level_group_path(assigns(:level_group))
  end

  test "should destroy level_group" do
    assert_difference('LevelGroup.count', -1) do
      delete :destroy, id: @level_group
    end

    assert_redirected_to level_groups_path
  end
end
