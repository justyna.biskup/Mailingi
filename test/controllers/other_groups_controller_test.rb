require 'test_helper'

class OtherGroupsControllerTest < ActionController::TestCase
  setup do
    @other_group = other_groups(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:other_groups)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create other_group" do
    assert_difference('OtherGroup.count') do
      post :create, other_group: { name: @other_group.name }
    end

    assert_redirected_to other_group_path(assigns(:other_group))
  end

  test "should show other_group" do
    get :show, id: @other_group
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @other_group
    assert_response :success
  end

  test "should update other_group" do
    patch :update, id: @other_group, other_group: { name: @other_group.name }
    assert_redirected_to other_group_path(assigns(:other_group))
  end

  test "should destroy other_group" do
    assert_difference('OtherGroup.count', -1) do
      delete :destroy, id: @other_group
    end

    assert_redirected_to other_groups_path
  end
end
