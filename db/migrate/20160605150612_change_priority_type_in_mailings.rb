class ChangePriorityTypeInMailings < ActiveRecord::Migration
  def up
    change_column :mailings, :priority, :string
  end

  def down
    change_column :mailings, :priority, :integer
  end
end