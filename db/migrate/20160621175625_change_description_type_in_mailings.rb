class ChangeDescriptionTypeInMailings < ActiveRecord::Migration

  def up
      change_column :mailings, :description, :text

      create_table :articles do |t|
        t.text :content
        t.string :title

        t.timestamps null: false
      end
  end

  def down
      change_column :mailings, :description, :string
  end


end