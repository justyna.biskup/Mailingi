class CreateMailingGroups < ActiveRecord::Migration
  def change
    create_table :mailing_groups do |t|
      t.integer :mailing_id
      t.integer :level_group_id
      t.integer :subject_group_id
      t.integer :other_group_id

      t.timestamps null: false
    end
  end
end
