class CreateLevelGroups < ActiveRecord::Migration
  def change
    create_table :level_groups do |t|
      t.string :name

      t.timestamps
    end
  end
end
