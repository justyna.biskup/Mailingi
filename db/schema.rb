# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160911180935) do

  create_table "articles", force: :cascade do |t|
    t.text     "content"
    t.string   "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "audits", force: :cascade do |t|
    t.integer  "auditable_id"
    t.string   "auditable_type"
    t.integer  "associated_id"
    t.string   "associated_type"
    t.integer  "user_id"
    t.string   "user_type"
    t.string   "username"
    t.string   "action"
    t.text     "audited_changes", limit: 16777215
    t.integer  "version",                          default: 0
    t.text     "comment"
    t.string   "remote_address"
    t.string   "request_uuid"
    t.datetime "created_at"
  end

  add_index "audits", ["associated_id", "associated_type"], name: "associated_index"
  add_index "audits", ["auditable_id", "auditable_type"], name: "auditable_index"
  add_index "audits", ["created_at"], name: "index_audits_on_created_at"
  add_index "audits", ["request_uuid"], name: "index_audits_on_request_uuid"
  add_index "audits", ["user_id", "user_type"], name: "user_index"

  create_table "group_files", force: :cascade do |t|
    t.integer  "mailing_id"
    t.integer  "group_size"
    t.string   "file_file_name"
    t.string   "file_content_type"
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "group_files", ["mailing_id"], name: "index_group_files_on_mailing_id"

  create_table "level_groups", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "mailing_files", force: :cascade do |t|
    t.integer  "mailing_id"
    t.string   "file_file_name"
    t.string   "file_content_type"
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "mailing_files", ["mailing_id"], name: "index_mailing_files_on_mailing_id"

  create_table "mailing_groups", force: :cascade do |t|
    t.integer  "mailing_id"
    t.integer  "level_group_id"
    t.integer  "subject_group_id"
    t.integer  "other_group_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "mailing_groups", ["level_group_id"], name: "index_mailing_groups_on_level_group_id"
  add_index "mailing_groups", ["mailing_id"], name: "index_mailing_groups_on_mailing_id"
  add_index "mailing_groups", ["other_group_id"], name: "index_mailing_groups_on_other_group_id"
  add_index "mailing_groups", ["subject_group_id"], name: "index_mailing_groups_on_subject_group_id"

  create_table "mailing_statuses", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "mailings", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.date     "send_date"
    t.date     "write_date"
    t.date     "code_date"
    t.date     "group_date"
    t.integer  "status_id"
    t.string   "preview"
    t.integer  "assign_to"
    t.integer  "author"
    t.string   "priority"
    t.boolean  "has_group",                  default: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "preview_image_file_name"
    t.string   "preview_image_content_type"
    t.integer  "preview_image_file_size"
    t.datetime "preview_image_updated_at"
    t.text     "other_group"
  end

  add_index "mailings", ["assign_to"], name: "index_mailings_on_assign_to"
  add_index "mailings", ["author"], name: "index_mailings_on_author"
  add_index "mailings", ["priority"], name: "index_mailings_on_priority"
  add_index "mailings", ["send_date"], name: "index_mailings_on_send_date"
  add_index "mailings", ["status_id"], name: "index_mailings_on_status_id"

  create_table "other_groups", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "subject_groups", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "user_groups", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.integer  "group_id"
    t.string   "password_digest"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["group_id"], name: "index_users_on_group_id"

end
